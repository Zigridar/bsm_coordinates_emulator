FROM node:14-alpine
COPY . ./app
RUN cd ./app && npm i && npm i cross-env -g && npm run build
EXPOSE 8080
CMD cd ./app && cross-env NODE_ENV="production" PORT=8080 MONGO_URI="$MONGO_URI" BACK_UP_DIR="$BACK_UP_DIR" node ./dist/app.js