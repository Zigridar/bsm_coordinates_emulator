import {expect} from 'chai';
import {describe} from 'mocha';
import {assertGet} from '../config';

const testConfig = {
  numberProperty: 10,
  stringProperty: 'test',
  booleanProperty: true,
  objectProperty: {}
}

describe('extract number', () => {
  const result = assertGet<number>('numberProperty', testConfig)
  expect(result).a('number')
});

describe('extract string', () => {
  const result = assertGet<string>('stringProperty', testConfig)
  expect(result).a('string')
});
describe('extract boolean', () => {
  const result = assertGet<boolean>('booleanProperty', testConfig)
  expect(result).a('boolean')
});

describe('extract object', () => {
  const result = assertGet<object>('objectProperty', testConfig)
  expect(result).a('object')
});