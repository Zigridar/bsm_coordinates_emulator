/** get source from env variable with assert if not provided */
export const assertGet: <T>(property: string, source: any) => T = <T>(property: string, source: any) => {
    const data = source[property] as any as T
    if (data)
        return data
    else
        throw new Error(`${property} isn't provided!`)
}

const config = () => ({
    PORT: assertGet<number>('PORT', process.env),
    MONGO_URI: assertGet<string>('MONGO_URI', process.env),
    backupDir: assertGet<string>('BACK_UP_DIR', process.env)
})

export default config